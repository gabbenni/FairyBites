﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

	public GameObject screamPrefab; 

	public Transform screamSpawn;

	public float speed;             //Floating point variable to store the player's movement speed.

	private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.

	// Use this for initialization
	void Start()
	{
		//Get and store a reference to the Rigidbody2D component so that we can access it.
		rb2d = GetComponent<Rigidbody2D> ();
	}

	//FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
	void FixedUpdate()
	{
		//Store the current horizontal input in the float moveHorizontal.
		float moveHorizontal = Input.GetAxis ("Horizontal");

		//Store the current vertical input in the float moveVertical.
		float moveVertical = Input.GetAxis ("Vertical");

		//Use the two store floats to create a new Vector2 variable movement.
		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);

		//Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
		rb2d.AddForce (movement * speed);
	}


	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space))
		{
			Fire ();
		}




	}

	void Fire()
	{
		GameObject scream = (GameObject)Instantiate (
			             screamPrefab,
			             screamSpawn.position,
			             screamSpawn.rotation);
		//scream.GetComponent<Rigidbody2D>().velocity = scream.transform.forward * 10;

		Rigidbody rb = scream.GetComponent<Rigidbody> ();

		rb.AddForce (Vector3.right * 500);

		//scream.GetComponent<Rigidbody2D>().AddForce(scream.transform.forward * 6);

		//print (scream.GetComponent<Rigidbody2D> ().velocity);

		Destroy (scream, 2.0f);
			
	}
}





